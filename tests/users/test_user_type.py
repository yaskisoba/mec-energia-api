import pytest
from users import models
from utils.user.user_type_util import UserType

class TestUserType:
    def test_wrong_user_type_in_UniversityUser(self):
        try:
            UserType.is_valid_user_type(models.CustomUser.super_user_type, models.UniversityUser)
        except Exception as e:
            assert str(e) == "Wrong User type (super_user) for this Model User (<class 'users.models.UniversityUser'>)"

    def test_correct_user_type_and_UniversityUser (self):
        user_type = UserType.is_valid_user_type(models.CustomUser.university_user_type, models.UniversityUser)
        assert user_type == "university_user"

    def test_wrong_model_right_user_type(self):
        user_type = UserType.is_valid_user_type(models.CustomUser.super_user_type, "lalala")
        assert user_type == "super_user"

    def test_right_model_wrong_user_type(self):
        try:
            UserType.is_valid_user_type("yas_user", models.CustomUser)
        except Exception as e:
            assert str(e) == "User type (yas_user) does not exist"

    def test_model_custom_and_user_type_is_superuser(self):
        user_type = UserType.is_valid_user_type(models.CustomUser.super_user_type, "lalala")
        assert user_type == "super_user"

    def test_wrong_model_and_user_type_is_not_superuser(self):
        try:
            UserType.is_valid_user_type("yas_user", "lalala")
        except Exception as e:
            assert str(e) == "User type (yas_user) does not exist"
